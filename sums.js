function sums(maximum) {
    let result = 0;
    while (--maximum >= 3) {
        if (isMultipleOf3(maximum) || isMultipleOf5(maximum)) {
            result += maximum;
        }
    }
    return result;
}

function isMultipleOf3(maximum) {
    return maximum % 3 == 0
}

function isMultipleOf5(maximum) {
    return maximum % 5 == 0
}

module.exports = sums;