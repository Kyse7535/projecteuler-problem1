const sums = require("./sums")

test("sum of number lt 3 should return 0", () => {
    expect(sums(3)).toBe(0);
});

test("sum of number lt 4 should return 3", () => {
    expect(sums(4)).toBe(3);
});

test("sum of number lt 5 should return 3", () => {
    expect(sums(5)).toBe(3);
});

test("sum of number lt 6 should return 8", () => {
    expect(sums(6)).toBe(8);
});

test("sum of number gt 7 should return 14", () => {
    expect(sums(7)).toBe(14);
});

test("sum of number lt 8 should return 14", () => {
    expect(sums(8)).toBe(14);
});

test("sum of number lt 9 should return 14", () => {
    expect(sums(9)).toBe(14);
});

test("sum of number lt 10 should return 23", () => {
    expect(sums(10)).toBe(23);
});

test("sum of number lt 11 should return 33", () => {
    expect(sums(11)).toBe(33);
});

test("sum of number lt 17 should return 60", () => {
    expect(sums(17)).toBe(60);
});